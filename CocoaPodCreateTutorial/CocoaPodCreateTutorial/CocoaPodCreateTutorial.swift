import Foundation

public final class CocoaPodCreateTutorial {

    let name = "CocoaPodCreateTutorial"

    public static func add(a: Int, b: Int) -> Int {
        return a + b
    }

    public static func sub(a: Int, b: Int) -> Int {
        return a - b
    }
}
