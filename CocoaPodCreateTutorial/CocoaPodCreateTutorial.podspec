Pod::Spec.new do |spec|

  spec.name         = "CocoaPodCreateTutorial"
  spec.version      = "0.0.1"
  spec.summary      = "CocoaPodCreateTutorial Tutorial"

  spec.description  = <<-DESC
 Это мы научились делать свой под
                   DESC

  spec.homepage     = "https://gitlab.com/example/cocoapodcreatetutorial"

	
  spec.ios.deployment_target = "12.1"
  spec.swift_version = "4.2"

  spec.license      = { :type => "MIT", :file => "LICENSE" }
  spec.author             = { "12dot" => "example@gmail.com" }
  spec.source       = { :git => "https://gitlab.com/example/cocoapodcreatetutorial", :tag => "#{spec.version}" }

  spec.source_files  = "CocoaPodCreateTutorial/**/*.{h,m,swift}"

end