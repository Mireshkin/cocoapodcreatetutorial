import Charts
import SwiftUI

struct Chart: UIViewRepresentable {

    func updateUIView(_ uiView: PieChartView, context: Context) {
    }

    typealias UIViewType = PieChartView

    static let car = SpendingModel(
        cashValue: 40000.0,
        spendingName: "Urus"
    )

    static let iPhone = SpendingModel(
        cashValue: 1000.0,
        spendingName: "iPhone XR"
    )

    static let internet = SpendingModel(
        cashValue: 15.0,
        spendingName: "MTS"
    )

    static let house = SpendingModel(
        cashValue: 100000.0,
        spendingName: "Dream Team House"
    )

    static let travel = SpendingModel(
        cashValue: 30000.0,
        spendingName: "Germany visit"
    )

    var entries: [PieChartDataEntry] = [
        .init(value: car.cashValue, label: car.spendingName),
        .init(value: iPhone.cashValue, label: iPhone.spendingName),
        .init(value: internet.cashValue, label: internet.spendingName),
        .init(value: house.cashValue, label: house.spendingName),
        .init(value: travel.cashValue, label: travel.spendingName)
    ]

    func addData() -> PieChartData {
        let dataSet = PieChartDataSet(entries: entries, label: "My Spending")
        dataSet.colors = makeColors(count: entries.count)
        let data = PieChartData(dataSet: dataSet)
        return data
    }

    func makeColors(count: Int) -> [UIColor] {
        var colors: [UIColor] = []
        for _ in 0...count {
            let color = UIColor.init(
                red: CGFloat.random(in: 0...1),
                green: CGFloat.random(in: 0...1),
                blue: CGFloat.random(in: 0...1),
                alpha: 1
            )
            colors.append(color)
        }
        return colors
    }

    func makeUIView(context: Context) -> PieChartView {
        let chart = PieChartView()
        chart.data = addData()
        return chart
    }

}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        Chart()
    }
}
