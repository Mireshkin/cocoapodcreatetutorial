//
//  ChartTutorialApp.swift
//  ChartTutorial
//
//  Created by Ilya Mireshkin on 02.12.2021.
//

import SwiftUI

@main
struct ChartTutorialApp: App {
    var body: some Scene {
        WindowGroup {
            Chart()
        }
    }
}
